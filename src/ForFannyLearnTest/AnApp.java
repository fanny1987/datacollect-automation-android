package ForFannyLearnTest;

import org.junit.After;
import org.junit.Before;

import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

@SuppressWarnings("rawtypes")
public class AnApp extends ActivityInstrumentationTestCase2{

	public  Solo solo;
	private static String PackName = "com.skf.datacollect.ceo.dev";
    private static String mainActivity1 = "com.skf.datacollect.module.entry.WelcomeActivity";
    private static Class<?> launchActivityClass;
    
    static{
    	 
        try{
            launchActivityClass = Class.forName(mainActivity1);
        }catch(ClassNotFoundException e){
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
	public AnApp() {
    	super(PackName,launchActivityClass);
    	// TODO Auto-generated constructor stub
    }

	@Before
	public void setUp() throws Exception {
		try{
			super.setUp();
			solo = new Solo(getInstrumentation(), getActivity());
		} catch(Throwable tr){
			throw new SetUpException(tr);
		}
		
	}

	@After
	public void tearDown() throws Exception {
		try{
			//solo.finishOpenedActivities();
			super.tearDown();
		}catch(Throwable th){
			solo.takeScreenshot(this.getClass().getSimpleName());
			throw new TearDownException(th);
		}
		finally{
			solo.finishOpenedActivities();
			}
	}
	class SetUpException extends Exception {
		private static final long serialVersionUID = 1L;
		SetUpException(Throwable e) {
		super("Error in BasicTestCase.setUp()!", e);
		}
		}
	class RunTestException extends Exception {
		private static final long serialVersionUID = 2L;
		RunTestException(Throwable e) {
		super("Error in BasicTestCase.runTest()", e);
		}
		}
		class TearDownException extends Exception {
		private static final long serialVersionUID = 3L;
		TearDownException(Throwable e) {
		super("Error in MultiTestCase.tearDown()", e);
		}
		}
}