package com.ceo.dc.android.util;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

@SuppressWarnings("rawtypes")
public class BasicTestCase extends ActivityInstrumentationTestCase2 {
	
	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME="com.skf.datacollect.module.entry.WelcomeActivity";
	private static Class<?> launcherActivityClass;
	static{
		try{
			launcherActivityClass=Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
		}catch(ClassNotFoundException e){
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public BasicTestCase(){
		super(launcherActivityClass);
	}
	
	public Solo solo;
	
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());

    }
    
    @Override
    public void tearDown(){
    	solo.finishOpenedActivities();
    }
    
	public void loginSucess(){
		 ElementsLoginActivity la=new ElementsLoginActivity(solo); 
		 //check if it was at the login page, need add some codes (if)
		 la.initViewsForLogin();
		 la.enterName("fanny.gao@ceosoftcenters.com");
		 la.enterPassword("123456");
		 la.clickLoginButton();
	}
	
	public void logOut(){
		 ElementsDashboardActivity da=new ElementsDashboardActivity(solo); 
		 da.clickImageBtn();
		 da.initViewsForLeftBar();
		 da.clickLogOut();
		 da.clickOK();
		 solo.sleep(5000);
	}

}
