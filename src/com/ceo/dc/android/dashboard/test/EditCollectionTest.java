package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class EditCollectionTest extends BasicTestCase {
	public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		   da.clickOption();
		    solo.sleep(5000);
		    da.clickEditCollection();
		    assertTrue(solo.searchText("Remove Image",true));
		    solo.sleep(3000);
		    da.clickOngoing();
		    solo.sleep(3000);
	    	logOut();	
	    }

}
