package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckTouchSettingTest extends BasicTestCase {
	public void test(){
	    loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(3000);
	    da.clickImageBtn();
	    solo.sleep(3000);
	    da.initViewsForLeftBar();
	    solo.sleep(3000);
	    da.clickSettingTab();
	    solo.sleep(3000);
    	assertTrue(solo.searchText("You are using version"));
    	logOut();
    }
}
