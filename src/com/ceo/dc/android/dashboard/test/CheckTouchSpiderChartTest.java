package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckTouchSpiderChartTest extends BasicTestCase {
	public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		   da.clickOption();
		    solo.sleep(5000);
		    da.clickSpiderChart();
		    solo.sleep(3000);
		    assertTrue(solo.searchText("Ongoing",false));
		    solo.clickOnScreen(1400, 1600);
		    solo.sleep(3000);
		    assertTrue(solo.searchText("Ongoing",true));
		    solo.sleep(3000);
	    	logOut();	
	    }
}
