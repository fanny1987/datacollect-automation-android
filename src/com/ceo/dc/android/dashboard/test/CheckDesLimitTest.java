package com.ceo.dc.android.dashboard.test;

import android.util.Log;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckDesLimitTest extends BasicTestCase {
	public void test(){
		
	    loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.clickImageBtn();
	    solo.sleep(3000);
	    da.initViewsForLeftBar();
	    solo.sleep(3000);
	    da.clickAddCoTab();
	    solo.sleep(3000);
	    da.clickAddCollection();
	    solo.sleep(3000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
	    da.enterCoDes("testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		+ "testkjhgdbctjhgjhghfdsfhgdsffhgfdshfdhshfggflkjdhrtdnchagsdlkfhejwtqiegcnsbxfsrwmchstedkflsgtehsnmchgdsg"
	    		);
	    solo.sleep(3000);
	    da.clickOngoing();
	    int length=da.getDesTextLength();   
	    String lengthForString=length+"";
	    Log.v("length",lengthForString);
	    assertTrue(length==2000);	
    	solo.sleep(3000);
    	da.clickOption();
    	solo.sleep(5000);
    	da.clickDeleteCollection();
    	solo.sleep(3000);
    	da.clickConfirmDelete();
    	solo.sleep(3000);
    	logOut();
		
		//Log.d("length","12345678");
    }
    
}
