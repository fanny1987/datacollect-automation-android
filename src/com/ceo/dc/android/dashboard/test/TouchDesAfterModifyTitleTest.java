package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class TouchDesAfterModifyTitleTest extends BasicTestCase {
	public void test(){
	    loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
	    da.clickOption();
	    solo.sleep(3000);
	    da.clickEditCollection();
	    solo.sleep(3000);
	    da.enterCoTitle("test");
	    solo.sleep(3000);
	    da.clickCoDes();
	    solo.sleep(3000);
	    assertTrue(da.desStatus());
    	assertTrue(solo.searchText("test"));
    	da.enterCoTitle("No name 2");
    	solo.sleep(3000);
    	da.clickOngoing();
    	solo.sleep(3000);
    	logOut();
    }
}
