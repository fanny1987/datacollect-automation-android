package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class LongPressCollectionTest extends BasicTestCase {
	public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		   da.clickLongOnCo();
		    solo.sleep(5000);
		    assertTrue(solo.searchText("Delete",true));
	    	logOut();	
	    }

}
