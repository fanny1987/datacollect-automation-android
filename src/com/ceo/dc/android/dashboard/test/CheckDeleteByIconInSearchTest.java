package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckDeleteByIconInSearchTest extends BasicTestCase {
	public void test(){
		
	    loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
	    da.clickSearchBtn();
	    solo.sleep(3000);
	    da.initViewsForSearch();
	    solo.sleep(3000);
	    da.enterSearchText("test");
	    solo.sleep(3000);
	   boolean flag=solo.searchText("f003",true);
	    //Log.d("search",flag);
	    assertFalse(flag);
	    da.clickCloseBtn();
	    solo.sleep(3000);
	    assertTrue(solo.searchText("f003",true));
    	logOut();
		
		
    }
}
