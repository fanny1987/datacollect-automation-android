package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class DeleteSearchedCoInCompletedTest extends BasicTestCase {
	public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.clickImageBtn();
	    solo.sleep(3000);
	    da.initViewsForLeftBar();
	    solo.sleep(3000);
	    da.clickAddCoTab();
	    solo.sleep(3000);
	    da.clickAddCollection();
	    solo.sleep(3000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		    da.clickSearchBtn();
		    solo.sleep(3000);
		    da.initViewsForSearch();
		    solo.sleep(3000);
		    da.clickOngoing();
		    solo.sleep(3000);
		    da.enterSearchText("no name 3");
		    da.clickOption();
		    da.clickSetComplete();
		    da.clickYesText();
		    solo.sleep(3000);
		    da.clickCompleted();
		    solo.sleep(3000);
		    da.clickOption();
	    	solo.sleep(5000);
	    	da.clickDeleteCollection();
	    	solo.sleep(3000);
	    	da.clickConfirmDelete();
	    	solo.sleep(3000);
	    	assertFalse(solo.searchText("f003",true));
	    	logOut();	
	    }
}
