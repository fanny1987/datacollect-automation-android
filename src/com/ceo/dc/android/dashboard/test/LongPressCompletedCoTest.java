package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class LongPressCompletedCoTest extends BasicTestCase {
	public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.clickCompleted();
	    solo.sleep(3000);
		   da.clickLongOnCompletedCo();
		    solo.sleep(5000);
		    assertTrue(solo.searchText("Delete",true));
	    	logOut();	
	    }
}
