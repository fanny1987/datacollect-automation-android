package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class SetCoAfterSearchTest extends BasicTestCase {
	public void test(){
		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		    da.clickSearchBtn();
		    solo.sleep(3000);
		    da.initViewsForSearch();
		    solo.sleep(3000);
		    da.enterSearchText("no name 1");
		    da.clickOption();
		    da.clickSetComplete();
		    da.clickYesText();
		    solo.sleep(3000);
		    da.clickCompleted();
		    assertTrue(solo.searchText("f003",true));
		    solo.sleep(3000);
		    da.clickOption();
		    da.clickSetOngoing();
		    solo.sleep(3000);
		    da.clickOngoing();
	    	solo.sleep(3000);
	    	assertTrue(solo.searchText("f003",true));
	    	da.initViewsForSingleUploadIcon();
	    	solo.sleep(6000);
	    	logOut();	
	    }

}
