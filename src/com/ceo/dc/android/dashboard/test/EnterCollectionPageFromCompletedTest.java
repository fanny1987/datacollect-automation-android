package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class EnterCollectionPageFromCompletedTest extends BasicTestCase {
public void test(){
		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		    da.clickSearchBtn();
		    solo.sleep(5000);
		    da.enterSearchText("Test1");
		    da.clickCompleted();
		    solo.sleep(3000);
		    da.clickCompletedCo();
		    solo.sleep(3000);
		    assertTrue(solo.searchText("All Question types",true));
		    solo.sleep(3000);
		    solo.goBack();
		    solo.sleep(3000);
	    	logOut();	
	    }
	
}
