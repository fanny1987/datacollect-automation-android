package com.ceo.dc.android.dashboard.test;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckOptionItemTest extends BasicTestCase {
public void test(){		
		loginSucess();
	    ElementsDashboardActivity da=new ElementsDashboardActivity(solo);
	    solo.sleep(5000);
	    da.initViewsForColletion();
	    solo.sleep(3000);
		   da.clickOption();
		    solo.sleep(5000);
		    assertTrue(solo.searchText("Set to complete",true));
		    assertTrue(solo.searchText("SpiderChart Title",true));
		    assertTrue(solo.searchText("Generate Report",true));
		    assertTrue(solo.searchText("Edit Collection",true));
		    assertTrue(solo.searchText("Delete Collection",true));
		    assertTrue(solo.searchText("Set media always downloaded during sync",true));
		    solo.sleep(3000);
		    solo.goBack();
		    solo.sleep(3000);
	    	logOut();	
	    }
}
