package com.ceo.dc.android.login.test;

import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckResetWithWrongFormatTest extends BasicTestCase {
	 public void test(){
		    ElementsLoginActivity la=new ElementsLoginActivity(solo); 
		    la.initViewsForLogin();
		    la.clickForgotPwdLink();
		    la.initViewsForResetPwd();
		    la.enterResetEmail("987@163");
		    la.clickResetButton();
		    la.initViewsForResetMsg();
		    String text=la.getResetMessage();
	    	assertTrue(text.contains("Unable to send reset link"));
	    }
}
