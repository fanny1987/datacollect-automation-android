package com.ceo.dc.android.login.test;

import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckResetPasswordTest extends BasicTestCase {
	 public void test(){
		    ElementsLoginActivity la=new ElementsLoginActivity(solo); 
		    la.initViewsForLogin();
		    la.clickForgotPwdLink();
		    la.initViewsForResetPwd();
		    la.enterResetEmail("fanny.gao@ceosoftcenters.com");
		    la.clickResetButton();
		    la.initViewsForResetMsg();
		    String text=la.getResetMessage();
	    	assertTrue(text.contains("Password reset email sent"));
	    }
}
