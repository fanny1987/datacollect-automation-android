package com.ceo.dc.android.login.test;

import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class EnterForgotPwdTest extends BasicTestCase {
	 public void test(){
		    ElementsLoginActivity la=new ElementsLoginActivity(solo); 
		    la.initViewsForLogin();
		    la.clickForgotPwdLink();
		    solo.sleep(3000);
	    	assertTrue(solo.searchText("Request reset link"));
	    }
}
