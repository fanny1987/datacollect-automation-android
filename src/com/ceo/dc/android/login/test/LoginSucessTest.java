package com.ceo.dc.android.login.test;

import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class LoginSucessTest extends BasicTestCase {
	   public void test(){
		    ElementsLoginActivity la=new ElementsLoginActivity(solo); 
		    la.initViewsForLogin();
		    la.enterName("fanny.gao@ceosoftcenters.com");
		    la.enterPassword("123456");
		    la.clickLoginButton();
		    solo.sleep(3000);
	    	assertTrue(solo.searchText("My Collections"));
	    	logOut();
	    }
}
