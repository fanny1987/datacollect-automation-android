package com.ceo.dc.android.login.test;

import android.test.suitebuilder.annotation.Smoke;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckDefaultPageTest extends BasicTestCase {
	@Smoke
	 public void test(){
		    loginSucess();
		    ElementsDashboardActivity da=new ElementsDashboardActivity(solo); 
		    da.initViewsForColletion();
		    solo.sleep(3000);
		    da.clickOption();
	    	assertTrue(solo.searchText("Set to complete"));
	    	da.clickEditCollection();
	    	logOut();
	    }
}
