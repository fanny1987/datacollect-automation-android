package com.ceo.dc.android.login.test;

import android.test.suitebuilder.annotation.Smoke;

import com.ceo.dc.android.activity.ElementsDashboardActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckDefaultCompanyTest extends BasicTestCase {
	@Smoke
	 public void test(){
		    loginSucess();
		    ElementsDashboardActivity da=new ElementsDashboardActivity(solo); 
		    solo.sleep(3000);
		    da.clickImageBtn();
		    solo.sleep(3000);
		    da.initViewsForLeftBar();
		    solo.sleep(3000);
		    String text=da.getCompanyText();
	    	assertTrue(text.contains("Company for Fanny"));
	    	logOut();
	    }
}
