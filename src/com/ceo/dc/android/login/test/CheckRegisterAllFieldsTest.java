package com.ceo.dc.android.login.test;

import com.ceo.dc.android.activity.ElementsLoginActivity;
import com.ceo.dc.android.util.BasicTestCase;

public class CheckRegisterAllFieldsTest extends BasicTestCase {
	public void test(){
		String time=String.valueOf(System.currentTimeMillis()); 
	    ElementsLoginActivity la=new ElementsLoginActivity(solo); 
	    la.initViewsForLogin();
	    la.clickSignUpLink();
	    solo.sleep(3000);
	    la.initViewsForRegister();
	    la.enterEmail(time+"@163.com");
	    la.enterConfirmEmail(time+"@163.com");
	    la.enterPwd("123456");
	    la.enterConfirmPwd("123456");
	    la.enterUsername(time);
	    la.enterFirstname(time);
	    la.enterLastname(time);
	    la.clickSignUpButton();
	    solo.sleep(3000);
    	assertTrue(solo.searchText("Account created"));
    	la.clickProceedButton();
    	assertTrue(solo.searchButton("Login"));
    	la.initViewsForLogin();
    	String text=la.getName();
    	assertTrue(text.contains(time));
    }
}
