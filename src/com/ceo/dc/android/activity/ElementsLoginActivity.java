package com.ceo.dc.android.activity;

import android.widget.EditText;
import android.widget.TextView;

import com.robotium.solo.Solo;

public class ElementsLoginActivity {
	public Solo solo;
	
	public ElementsLoginActivity(Solo solo){
		this.solo=solo;
	}
	
	//all the Elements
	EditText nameEditText;
	EditText passwordEditText;
	String loginText;
	String signUpText;
	String forgotPwdText;
	EditText emailText;
	EditText confirmEmailText;
	EditText passwordText;
	EditText confirmPwdText;
	EditText usernameText;
	EditText firstnameText;
	EditText lastnameText;
	String signUpBtnText="Sign up";
	String proceedText="Proceed to login";
	EditText resetEmailText;
	String resetButton="Request reset link";
	TextView resetMessage;
	
	public void initViewsForLogin(){
		nameEditText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/email");
		passwordEditText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/password");
		loginText = "Login";
		signUpText = "Sign up for DataCollect from SKF";
		forgotPwdText = "I forgot my password";
	}
	
	public void initViewsForRegister(){
		emailText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/register_email");
		confirmEmailText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/confirm_email");
		passwordText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/register_password");
		confirmPwdText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/confirm_password");
		usernameText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/user_name");
		firstnameText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/first_name");
		lastnameText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/last_name");
	}
	
	public void initViewsForResetPwd(){
		resetEmailText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/edt_forgot_pass_email");
	}
	
	public void initViewsForResetMsg(){
	resetMessage=(TextView) solo.getView("com.skf.datacollect.ceo.dev:id/title");
	}
	
	public void enterName(String text){
		solo.enterText(nameEditText, text);
	}
	
	public String getName(){
		return nameEditText.getText().toString();
	}
	
	public void enterPassword(String text){
		solo.enterText(passwordEditText, text);
	}

	public void clickLoginButton(){
		solo.clickOnButton(loginText);
	}
	
	public void clickSignUpLink(){
		solo.clickOnText(signUpText);
	}
	
	public void clickForgotPwdLink(){
		solo.clickOnText(forgotPwdText);
	}
	
	public void enterEmail(String text){
		solo.enterText(emailText, text);
	}
	
	public void enterConfirmEmail(String text){
		solo.enterText(confirmEmailText, text);
	}
	
	public void enterPwd(String text){
		solo.enterText(passwordText, text);
	}
	
	public void enterConfirmPwd(String text){
		solo.enterText(confirmPwdText, text);
	}
	
	public void enterUsername(String text){
		solo.enterText(usernameText, text);
	}
	
	public void enterFirstname(String text){
		solo.enterText(firstnameText, text);
	}
	
	public void enterLastname(String text){
		solo.enterText(lastnameText, text);
	}
	
	public void clickSignUpButton(){
		solo.clickOnButton(signUpBtnText);
	}
	
	public void clickProceedButton(){
		solo.clickOnButton(proceedText);
	}
	
	public void enterResetEmail(String text){
		solo.enterText(resetEmailText, text);
	}
	
	public void clickResetButton(){
		solo.clickOnButton(resetButton);
	}
	
	public String getResetMessage(){
		return resetMessage.getText().toString();
	}

}
