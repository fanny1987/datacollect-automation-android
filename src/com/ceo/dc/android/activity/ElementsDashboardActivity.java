package com.ceo.dc.android.activity;

import android.support.v7.widget.SwitchCompat;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.robotium.solo.Solo;

public class ElementsDashboardActivity {
		public Solo solo;
		
		public ElementsDashboardActivity(Solo solo){
			this.solo=solo;
		}
		
		//all the Elements
		TextView companyText;
		String logOutText="Log out";
		String okText="OK";
		ImageView checkOption;
		String editCollectionText="Edit Collection";
		String woTab="My Work Orders";
		String coTab="My Collections";
		String addCoTab="Add a Collection";
		SwitchCompat managerContent;
		String settingText="Settings";
		String helpText="Help & support";
		ImageView searchIcon;
		String addCollection="Add Collection";
		EditText coTitle;
		EditText coDes;
		String deleteCollection="Delete Collection";
		String confirmDelete="Yes, delete";
		String ongoingText="Ongoing";
		String completedText="Completed";
		ImageView searchBtn;
		EditText searchText;
		ImageView closeBtn;
		String setComplete="Set to complete";
		String yesText="Yes";
		String setOngoing="Set to ongoing";
		ImageView singleUpload;
		String completedCo="Test1";
		String ongoingCo="No name 1";
		String spiderChartText="SpiderChart Title";
		
		public void initViewsForLeftBar(){
			companyText=(TextView) solo.getView("com.skf.datacollect.ceo.dev:id/company_name");
			managerContent= (SwitchCompat) solo.getView("com.skf.datacollect.ceo.dev:id/section_switch");
		}
		
		public void initViewsForColletion(){
		    checkOption=(ImageView) solo.getView("com.skf.datacollect.ceo.dev:id/work_order_item_iv_three_dots");
		    searchIcon=(ImageView) solo.getView("com.skf.datacollect.ceo.dev:id/search_button");
		    coTitle=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/form_list_item_et_title");
		    coDes=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/form_list_item_et_description");
		    searchBtn=(ImageView) solo.getView("com.skf.datacollect.ceo.dev:id/search_button");
		}
		
		public void initViewsForSearch(){
			searchText=(EditText) solo.getView("com.skf.datacollect.ceo.dev:id/search_src_text");
		    closeBtn=(ImageView) solo.getView("com.skf.datacollect.ceo.dev:id/search_close_btn");
		}
		
		public void initViewsForSingleUploadIcon(){
			singleUpload=(ImageView) solo.getView("com.skf.datacollect.ceo.dev:id/work_order_item_iv_arrow");
		}
		
		public void enterSearchText(String text){
			solo.enterText(searchText, text);
		}
		
		public void clickCloseBtn(){
			solo.clickOnView(closeBtn);
		}
		
		public void clickImageBtn(){
			solo.clickOnImageButton(0);
		}
		
		public void clickSearchBtn(){
			solo.clickOnView(searchBtn);
		}
		
		public String getCompanyText(){
			return (String) companyText.getText();
		}
		
		public void clickLogOut(){
			solo.clickOnText(logOutText);
		}

		public void clickOK(){
			solo.clickOnText(okText);
		}
		
		public void clickOption(){
			solo.clickOnView(checkOption);
		}
		
		public void clickEditCollection(){
			solo.clickOnText(editCollectionText);
		}
		
		public void clickWOTab(){
			solo.clickOnText(woTab);
		}
		
		public void clickCoTab(){
			solo.clickOnText(coTab);
		}
		
		public void clickAddCoTab(){
			solo.clickOnText(addCoTab);
		}
		
		public boolean getStatusOfManagerContent(){
			return managerContent.isChecked();
		}
		
		public void clickSettingTab(){
			solo.clickOnText(settingText);
		}
		
		public void clickHelpTab(){
			solo.clickOnText(helpText);
		}
		
		public void clickOtherField(){
			solo.clickOnScreen(1450, 866);
		}
		
		public void clickAddCollection(){
			solo.clickOnButton(addCollection);
		}
		
		public void enterCoTitle(String text){
			solo.clearEditText(coTitle);
			solo.sleep(3000);
			solo.enterText(coTitle, text);
		}
		
		public void enterCoDes(String text){
			solo.enterText(coDes, text);
		}
		
		public void clickCoDes(){
			solo.clickOnView(coDes);
		}
		
		public void clickSetComplete(){
			solo.clickOnText(setComplete);
		}
		
		public void clickSetOngoing(){
			solo.clickOnText(setOngoing);
		}
		
		public void clickYesText(){
			solo.clickOnText(yesText);
		}
		
		public boolean desStatus(){
			return coDes.isFocusable();
		}
		
		public int getDesTextLength(){
			String text= coDes.getText().toString();
			return text.length();
		}
		
		public void clickDeleteCollection(){
			solo.clickOnText(deleteCollection);
		}
		
		public void clickConfirmDelete(){
			solo.clickOnText(confirmDelete);
		}
		
		public void clickOngoing(){
			solo.clickOnText(ongoingText);
		}
		
		public void clickCompleted(){
			solo.clickOnText(completedText);
		}
		
		public String getSearchText(){
			return searchText.getText().toString();
		}
		
		public void clickCompletedCo(){
			solo.clickOnText(completedCo);
		}
		
		public void clickOngoingCo(){
			solo.clickOnText(ongoingCo);
		}
		
		public void clickLongOnCo(){
			solo.clickLongOnText(ongoingCo);
		}
		
		public void clickLongOnCompletedCo(){
			solo.clickLongOnText(completedCo);
		}
		
		public void clickSpiderChart(){
			solo.clickOnText(spiderChartText);
		}
}

